import deep_translator
from deep_translator import GoogleTranslator
import json5 as json
import random
import re
import sys
import time
import datetime
import asyncio

from pathlib import Path
from statistics import mean

date = datetime.datetime.now()
date = date.strftime('%d-%m-%Y')
inputFile = 'lang.json'
outputFile = f'enshitted-{date}.json'
if len(sys.argv) > 1:
	inputFile = sys.argv[1]
if len(sys.argv) > 2:
	outputFile = sys.argv[2]

targetLanguage = 'en'
times = 18		# iterations per mistranslation
queueDepth = 4	# number of threads to run (4 is safe, 5+ may rate limit)

random.seed()

languages = GoogleTranslator().get_supported_languages(as_dict=True)
timeStart = time.time()
totalStrings = 0
remainingStrings = 0
totalErrors = 0

def getTranslationResult(ttt, source = 'auto', target = targetLanguage, errors = 0, halt=False):
	if halt:
		return ttt, source, target, errors, halt
	try:
		result = GoogleTranslator(source=source, target=target).translate(ttt)
		return result, source, target, errors, halt
	except Exception as e:
		if isinstance(e, deep_translator.exceptions.TranslationNotFound): # no translation; try something else
			errors += 1
			source = 'auto'
			target = languages[random.choice(list(languages))]
			return getTranslationResult(ttt, source, target, errors)
		if isinstance(e, deep_translator.exceptions.NotValidPayload):
			errors += 1
			return getTranslationResult('?????', source, target, errors, True)
		else:
			print(str(e))
			errors += 1
			return getTranslationResult(ttt, source, target, errors)
	
def EnshitLoop(ttt):
	origin = ttt
	temp = ttt
	errors = 0
	languageList = [targetLanguage]
	if len([i for i in temp if i.isalpha()]) > 0:
		print('Translating "' + temp + '"')
		for x in range(times):
			s, source, target, err, halt = getTranslationResult(temp, 'auto', languages[random.choice(list(languages))])
			if halt:
				temp = ttt # just return the raw key if we've failed spectacularly
				break
			else:
				languageList.append(target)
				temp = s
			errors += err
		if temp != ttt:
			result, source, target, err, halt = getTranslationResult(temp, 'auto', targetLanguage)
			languageList.append(target)
			errors += err
			return result, errors, languageList
		else:
			print(f'translation failed for "{origin}"')
			return ttt, errors, languageList
	else:
		print(f'passing {origin}')
		return ttt, errors, None

def DoEnshitting(transArgs):
	data = transArgs['data']
	key = transArgs['key']

	global timeStart, totalStrings, remainingStrings, totalErrors, timeResults
	
	tts = time.time()
	origin = data[key]
	result, errors, languageList = EnshitLoop(data[key])
	if not result.isascii():
		while not result.isascii():
			print(f'retrying because not ascii: {result}')
			errors += 1
			result, err, languageList = EnshitLoop(data[key])
			errors += err
	if languageList != None:
		print(*languageList, sep = '->')

	remainingStrings -= 1
	totalErrors += errors
	elapsed = round(time.time() - tts)

	print('')
	print(origin, result, sep='\n->\n')
	print(f'\n--- Translation complete after {elapsed} seconds and {errors}/{totalErrors} errors. {remainingStrings} remain. ---')

	transArgs['queue'].remove(transArgs['task'])
	data[key] = result

def CollectStrings(data, tasks: list, **kwargs):
	if isinstance(data, (dict, list)):
		for k, v in (data.items() if isinstance(data, dict) else enumerate(data)):
			CollectStrings(v, tasks, key = k, prev = data)
	elif isinstance(data, str):
		key = kwargs['key']
		prev = kwargs['prev']
		if isinstance(key, str):
			if not key.startswith('META_'):
				tasks.append({'data': prev, 'key': key})
		else:
			tasks.append({'data': prev, 'key': key})

async def Enshittify(inputFile = 'lang.json', outputFile = 'badlytranslated.json'):
	global timeStart, totalStrings, remainingStrings, totalErrors
	with open(inputFile, encoding='utf-8') as f:
		data = json.load(f)
		tasks = []
		queue = []

		CollectStrings(data, tasks)
		numStrings = len(tasks)
		remainingStrings = numStrings

		print(f'{numStrings} fields to enshittify.')

		while len(tasks) > 0:
			transArgs = tasks[0]
			transArgs['queue'] = queue
			tasks.pop(0)
			task = asyncio.create_task( asyncio.to_thread(DoEnshitting, transArgs) )
			transArgs['task'] = task
			queue.append(task)
			while len(queue) > queueDepth - 1:
				await asyncio.sleep(1)
		while len(queue) > 0:
				await asyncio.sleep(1)

		with open(outputFile, 'w+', encoding='utf-8') as f:
			json.dump(data, f, ensure_ascii=False, indent=4, quote_keys=True, trailing_commas=False)

		timeEnd = time.time()
		seconds = timeEnd - timeStart

		print('Elapsed time:', str(datetime.timedelta(seconds=seconds)))
		print('Total errors:', totalErrors)
		print('--- ENSHITTIFICATION COMPLETE ---')

asyncio.run(Enshittify(inputFile, outputFile))