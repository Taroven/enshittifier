import json5 as json
import datetime
import sys

date = datetime.datetime.now()
date = date.strftime('%d-%m-%Y')
inputFile = f'enshitted-{date}.json'
outputFile = f'enshitted-{date}-skilldesc.json'
if len(sys.argv) > 1:
	inputFile = sys.argv[1]
if len(sys.argv) > 2:
	outputFile = sys.argv[2]

with open(inputFile, encoding='utf-8') as f:
	data = json.load(f)

items = data['item']
for key in items:
	if 'pickup' in items[key] and 'description' in items[key]:
		desc = items[key]['description']
		items[key]['pickup'] += f'\n\n{desc}'

with open(outputFile, 'w+', encoding='utf-8') as f:
	json.dump(data, f, ensure_ascii=False, indent=4, quote_keys=True, trailing_commas=False)