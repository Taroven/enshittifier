# Frequent Rain Problems
If you ever wanted to know what it would be like to translate Risk of Rain Returns from English to random languages 18 times and back, here's your answer.

If you just want the mistranslation and don't care about the how, snag the zip in the file list and plop the folder within into the `languages` folder of your RoRR install. Click the language selector at the top right of the main menu, look for the upside down smiley face, and enjoy.

Some of the languages don't autodetect well back to English and I didn't feel like finding every one and running them back through translation, so there will be some... non-words here and there. I did my best to fix formatting though.

# Misconceptions about sports
## Badly translate games
I make no claims about this code being clean, efficient, or good. But it does spit out some poor translations.

Python3 required with `deep_translate` and `json5` from pip.

## Usage
`python[3] enshittify.py [input.json] [output.json]`
Default input is `lang.json`, default output is `enshittified-[date].json`. Adjust the number of iterations by changing `times` near the top.

Runtime with Risk of Rain Returns English `lang.json` is a little over 6 hours with the included settings. You can potentially speed it up by adjusting the queue depth near the top (thus increasing the number of threads used), but be aware that `deep_translator` limits API requests to 5 per second. 4 threads is definitely safe, more may trigger an API rate limit and lock you out for an hour or two.

## Note
If the server's having a bad day and returns code 500, it just spews it into the translation results. Also, Google Translate has about an 80% success rate at preserving formatting tags and the many and varied `%s` identifiers, so if you want things to make sense at all it's a good idea to proofread the output.

Yes, I did that for the initial release. Yes, it took an hour. You're welcome. <3